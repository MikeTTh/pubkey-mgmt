#!/usr/bin/env bash

########################### Virtualenv setup ####################################

# create virtualenv if not present
[[ ! -d .venv ]] && python3 -m venv .venv

source .venv/bin/activate

pip3 install wheel
pip3 install ansible ansible-lint

########################### Ansible setup ####################################

ansible-galaxy install --force -r requirements.galaxy.yml

########################### Help ####################################

# to stay in our comfy virtualenv
exec "${SHELL:bash}"

